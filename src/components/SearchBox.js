import React from 'react';

const SearchBox = ({searchField}) => {
    return(
        <div className="pa2">
            <input 
            className="pa3 ba b--green bg-lightest-blue" 
            type="search" 
            placeholder="search family" 
            onChange={searchField}
            />
        </div>
    );
}

export default SearchBox;