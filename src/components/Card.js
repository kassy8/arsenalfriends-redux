import React from 'react';
import '../card.css';
import 'tachyons';


const get_team_badge = (club) => {
    return `http://icons.iconarchive.com/icons/giannis-zographos/english-football-club/32/${club}-icon.png`;
};
const get_nationality_badge = (nat) => {
    return `https://www.countryflags.io/${nat}/flat/32.png`;
};
const get_photo = (photo,id) => {
    if(photo === null){
        return `https://robohash.org/${id}?150x150`;
    }else{
        return photo;
    }
};


const Card = ({familymember,stats,stables,teamsupport}) => {
    return(
    <div className="bg-light-green dib br3 pa3 ma2 grow bw2 shadow-4">
        <div className="fl w-30">
            <h2>{stats.overall}</h2>
            <h2>{familymember.position}</h2>
            <img src={get_team_badge(teamsupport.badge)} alt={teamsupport.name} />
            <img src={get_nationality_badge(familymember.nationality)} alt={familymember.nationality+" flag"} />
        </div>
        <div className="fl w-70">
            <img className="profile-pic" src={get_photo(familymember.photo,familymember.id)} alt={familymember.name} />
            <h2>{familymember.name} {stables[familymember.stable]}</h2>
            <h3><em>"{familymember.catchphrase}"</em></h3>
            <ul>
                <li><strong>{stats.PAC}</strong> PAC</li>
                <li><strong>{stats.SHO}</strong> SHO</li>
                <li><strong>{stats.PAS}</strong> PAS</li>
                <li><strong>{stats.DRI}</strong> DRI</li>
                <li><strong>{stats.DEF}</strong> DEF</li>
                <li><strong>{stats.PHY}</strong> PHY</li>
            </ul>
        </div>
    </div>
    );
}

export default Card;