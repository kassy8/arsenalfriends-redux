import{ CHANGE_SEARCHFIELD } from './constants'

const initialState = {
    searchField: ''
}

export const searchFamilia = (state=initialState, action={}) => {
    switch(action.type){
        case CHANGE_SEARCHFIELD:
            return Object.assign({}, state, {searchField: action.payload})
            //OR = return {...state, {searchField: action.payload}}
        default: 
            return state;
    }
};