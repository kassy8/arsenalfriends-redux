
export const teams = [
    {
        id: 0,
        names: ["aston villa","villa","avfc"],
        badge: "Aston-Villa"
    },
    {
        id: 1,
        names: ["arsenal","afc","gooners"],
        badge: "Arsenal-FC"
    },
    {
        id: 2,
        names: ["liverpool","lfc","reds"],
        badge: "Liverpool-FC"
    },
    {
        id: 3,
        names: ["man city","city","citeh","mcfc"],
        badge: "Manchester-City"
    },
    {
        id: 4,
        names: ["man u","united","man united","man utd","mufc"],
        badge: "Manchester-United"
    },
    {
        id: 5,
        names: ["tottenham","spurs","spuds","thfc"],
        badge: "Tottenham-Hotspur"
    },
    {
        id: 6,
        names: ["chelsea","cfc"],
        badge: "Chelsea-FC"
    }
];

export const stables = [
    "Islip",
    "Siamuwele",
    "Iacco",
    "Mukuma"
];

export const lafamilia = [
    {
        id: 1,
        name: "Elia",
        nationality: "it",
        position: "RB",
        catchphrase: "Love you to the moon and back",
        stable: 0,
        photo: null
    },
    {
        id: 2,
        name: "Paul",
        nationality: "it",
        position: "RB",
        catchphrase: "I am Equally overly casual and Cool",
        stable: 2,
        photo: null
    },
    {
        id: 3,
        name: "Kassy",
        nationality: "zm",
        position: "RB",
        catchphrase: "Overly casual cool cat, that I am",
        stable: 1,
        photo: null
    },
    {
        id: 4,
        name: "Kaka",
        nationality: "zm",
        position: "RB",
        catchphrase: "I Love you",
        stable: 3,
        photo: null
    },
    {
        id: 5,
        name: "Tomato",
        nationality: "nz",
        position: "RB",
        catchphrase: "I love tonic",
        stable: 1,
        photo: null
    }
];