import React, {Component} from 'react';
import {connect} from 'react-redux';
import Familia from '../components/Familia';
import SearchBox from '../components/SearchBox';
import ErrorBoundry from '../components/ErrorBoundry';
import Scroll from '../components/Scroll';
import {lafamilia,stables,teams} from '../familydata';
import '../App.css';

import {setSearchField} from '../actions';


const mapStateToProps = state => {
    return {
        searchField: state.searchField
    }
}

const mapDispatchToProps = dispatch => {
    return{ 
        onSearchChange: (event) => dispatch(setSearchField(event.target.value))
    }
}


class App extends Component {
    constructor(){
        super();
        this.state = {
            fam: lafamilia
        }
    }

    componentDidMount(){
        //console.log(this.props.store.getState);
        fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json() )
    .then(users => { /*console.log(users); this.setState({lafamilia: users });*/ });
    }

    render(){
        const { fam } = this.state;
        const { searchField } = this.props;
        const filteredfam= fam.filter(member=>{
            return member.name.toLowerCase().includes(searchField.toLowerCase());
        });
        if(fam.length===0){
            return(<h1>Loading Tings</h1>);
        }else{
            return(<div className="tc">
            <h1 className="f1">La Familia de SEGA</h1>
            <SearchBox searchChange={searchField} />
            <Scroll>
                <ErrorBoundry>
                    <Familia 
                    famdata={filteredfam} 
                    stable_names={stables} 
                    supported_teams={teams} />
                </ErrorBoundry>
            </Scroll>
            </div>);
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);